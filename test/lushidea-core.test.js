'use strict';

const mock = require('egg-mock');

describe('test/lushidea-core.test.js', () => {
  let app;
  before(() => {
    app = mock.app({
      baseDir: 'apps/lushidea-core-test',
    });
    return app.ready();
  });

  after(() => app.close());
  afterEach(mock.restore);

  it('should GET /', () => {
    return app.httpRequest()
      .get('/')
      .expect('hi, lushideaCore')
      .expect(200);
  });
});
