'use strict';

exports.keys = '123456';

exports.mongoose = {
  client: {
    url: 'mongodb://127.0.0.1/ls_core_test',
    options: {},
  },
};

exports.lushideaCore = {
  root: 'root',
  user_key: 'username',
  user_profile: {
    first_name: {
      type: String,
      fuzzy: true,
    },
    last_name: {
      type: String,
      fuzzy: true,
    },
  },
};