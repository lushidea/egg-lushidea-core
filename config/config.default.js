'use strict';

/**
 * egg-lushidea-core default config
 * @member Config#lushideaCore
 * @property {String} SOME_KEY - some description
 */
exports.lushideaCore = {
  root: 'root',
  user_key: 'username',
  user_profile: {},
};
