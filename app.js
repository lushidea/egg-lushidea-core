'use strict';

const { accessibleRecordsPlugin } = require('@casl/mongoose');

module.exports = app => {
  app.mongoose.plugin(accessibleRecordsPlugin);
};
