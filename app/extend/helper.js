'use strict';

const bcrypt = require('bcrypt');

module.exports = {
  /**
   * Check if the string is an Email address
   * @param {string} str the string needs to be validated
   * @return {boolean} true if the string is an Email address, false if not
   */
  isEmail(str) {
    const regex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/; // eslint-disable-line
    return regex.test(str);
  },

  mongooseErrosToMessages(error) {
    return Object.keys(error.errors).map(key => {
      return {
        field: key,
        message: error.errors[key].message,
      };
    });
  },

  hashStr: async str => {
    const salt = await bcrypt.genSalt(10, 'a');
    const hash = await bcrypt.hash(str, salt);
    return hash;
  },

  getParamsObject(params, keys = []) {
    const result = {};
    keys.forEach(k => {
      if (params[k] != undefined) {
        result[k] = params[k];
      }
    });
    return result;
  },

  getErrorResponse(ctx, code, message, errors = []) {
    ctx.status = code;
    ctx.body = {
      code,
      message,
      errors,
    };
  },
};
