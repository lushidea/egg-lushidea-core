'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const RoleSchema = new Schema({
    name: {
      type: String,
    },
    description: {
      type: String,
    },
  });
  return mongoose.model('Role', RoleSchema);
};
