'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const tokenSchema = {
    token: {
      type: String,
    },
    until: {
      type: Number,
    },
  };

  const schema = {
    status: {
      type: String,
      lowercase: true,
      trim: true,
      enum: [ 'enabled', 'disabled', 'deleted' ],
    },
    password: {
      type: String,
    },
    role: {
      type: String,
    },
    tokens: [ tokenSchema ],
  };
  if (app.config.lushideaCore.user_key === 'username') {
    schema.username = {
      type: String,
      required: [ true, 'Username is required' ],
      lowercase: true,
      unique: true,
      fuzzy: true,
    };
  } else if (app.config.lushideaCore.user_key === 'email') {
    schema.email = {
      type: String,
      required: [ true, 'Email address is required' ],
      lowercase: true,
      unique: true,
      fuzzy: true,
      validate: [ app.ctx.helper.isEmail, 'Please fill a valid email address' ],
    };
  }

  const UserSchema = new Schema(schema);

  return mongoose.model('User', UserSchema);
};
