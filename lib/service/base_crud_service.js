'use strict';

const Service = require('egg').Service;
const VerificationError = require('./../error/verification_error');

class BaseCRUDService extends Service {
  constructor(model) {
    super();
    this.model = model;
  }

  async create(params) {
    await this.checkUniqueFields(params);
    let entity = new this.model(params);
    let errors = entity.validateSync();
    if (errors) {
      errors = this.ctx.helper.mongoosErrorsToMessages(errors);
      throw new VerificationError(`Create ${this.model.modelName.toLowerCase()} failed`, errors);
    }
    entity = await entity.save();
    return await this.model.accessibleBy(this.ctx.state.ability).findOne({_id: entity.id.toString()}, this.ctx.state.fields);
  }

  async update(id, params) {
    // check unique fields
    await this.checkUniqueFields(params, id);
    const entity = await this.model.findById(id);
    if (!entity) {
      throw new VerificationError(`The ${this.model.modelName.toLowerCase()} doesn\'t exist`, []);
    }
    Object.keys(params).forEach(p => {
      entity[p] = params[p];
    });
    let errors = entity.validateSync();
    if (errors) {
      errors = this.ctx.helper.mongoosErrorsToMessages(errors);
      throw new VerificationError(`Update ${this.model.modelName.toLowerCase()} failed`, errors);
    }
    await entity.save();
    return await this.model.accessibleBy(this.ctx.state.ability).findOne({_id: id}, this.ctx.state.fields);
  }

  async find(params, skip, limit, extraConditions, sort) {
    const where = {};
    Object.keys(params).forEach(p => {
      if (params[p] instanceof Array) {
        where[p] = { $in: params[p] };
      } else if (params[p] !== undefined && this.model.schema.paths[p].instance == 'String' && this.model.schema.paths[p].options.fuzzy) {
        where[p] = new RegExp(params[p], 'i');
      } else if (params[p] !== undefined) {
        where[p] = params[p];
      }
    });
    let query = this.model.accessibleBy(this.ctx.state.ability).where(where).select(this.ctx.state.fields);
    if (extraConditions) {
      query = query.and([ where, extraConditions ]);
    }
    const count = await this.model.countDocuments(query.getQuery());
    if (sort) {
      query.sort(sort);
    }
    if (limit != null && limit !== undefined && !isNaN(limit)) {
      query.limit(Number(limit));
    }
    if (skip != null && skip !== undefined && !isNaN(skip)) {
      query.skip(Number(skip));
    }
    const data = await query;
    return { count, data };
  }

  async findById(id) {
    return await this.model.accessibleBy(this.ctx.state.ability).findOne({ _id: id }, this.ctx.state.fields);
  }

  async deleteByIds(ids = []) {
    await this.model.deleteMany({ _id: { $in: ids } });
  }

  async checkUniqueFields(params = {}, id = null) {
    const fields = Object.keys(this.model.schema.paths);
    const uniqueFields = fields.filter(f => this.model.schema.paths[f].options.unique).map(f => ({[f]: params[f]}));

    const exisitingFields = [];
    for (let i = 0; i < uniqueFields.length; i++) {
      const query = uniqueFields[i];
      if (id) {
        query._id = {
          $ne: id,
        };
      }
      const entities = await this.model.find(query);
      if (entities.length > 0) {
        exisitingFields.push(uniqueFields[i]);
      }
    }
    if (exisitingFields.length > 0) {
      const errors = exisitingFields.map(item => ({
        field: Object.keys(item)[0],
        message: `'${item[Object.keys(item)[0]]}' already exists`,
      }));
      throw new VerificationError(`Update ${this.model.modelName.toLowerCase()} failed`, errors);
    }
  }
}

module.exports = BaseCRUDService;